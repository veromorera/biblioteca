package bl;

import java.time.LocalDate;

public class Profesor extends Persona{

    private String tipoContrato;
    private LocalDate fechaContrato;


    //constructor
    public Profesor(int id, String nombre, String apellido, String tipoContrato, LocalDate fechaContrato) {
        super(id, nombre, apellido);

        this.tipoContrato = tipoContrato;
        this.fechaContrato = fechaContrato;
    }


    //getters
    public String getTipoContrato() {
        return tipoContrato;
    }
    public LocalDate getFechaContrato() {
        return fechaContrato;
    }


    //setters
    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }
    public void setFechaContrato(LocalDate fechaContrato) {
        this.fechaContrato = fechaContrato;
    }


    //toString
    @Override
    public String toString() {
        return "Profesor{" +
                "id=" + getId() +  '\'' +
                ", nombre=" + getNombre() +  '\'' +
                ", apellido=" + getApellido() +  '\'' +
                ", tipoContrato=" + tipoContrato +
                ", fechaContrato=" + fechaContrato +
                '}';
    }
}
