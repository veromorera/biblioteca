package bl;

public class Estudiante extends Persona {

    private String carrera;
    private int creditos;


    //constructor
    public Estudiante(int id, String nombre, String apellido, String carrera, int creditos) {
        super(id, nombre, apellido);

        this.carrera = carrera;
        this.creditos = creditos;
    }


    //getters
    public String getCarrera() {
        return carrera;
    }
    public int getCreditos() {
        return creditos;
    }


    //setters
    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }
    public void setCreditos(int creditos) {
        this.creditos = creditos;
    }


    //toString
    @Override
    public String toString() {
        return "Estudiante{" +
                "id=" + getId() +  '\'' +
                ", nombre=" + getNombre() +  '\'' +
                ", apellido=" + getApellido() +  '\'' +
                ", carrera='" + carrera + '\'' +
                ", creditos=" + creditos +
                '}';
    }
}
