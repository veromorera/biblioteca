package bl;

import java.time.LocalDate;

public class Texto extends Material {


    private String titulo;
    private String autor;
    private LocalDate fechaPublicacion;
    private int numeroPaginas;
    private String idioma;

    //constructor
    public Texto(String signatura, LocalDate fechaCompra, String restringido, String tema) {
        super(signatura, fechaCompra, restringido, tema);
    }

    //getters
    public String getTitulo() {
        return titulo;
    }

    public String getAutor() {
        return autor;
    }

    public LocalDate getFechaPublicacion() {
        return fechaPublicacion;
    }

    public int getNumeroPaginas() {
        return numeroPaginas;
    }

    public String getIdioma() {
        return idioma;
    }

    //setters

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setFechaPublicacion(LocalDate fechaPublicacion) {
        this.fechaPublicacion = fechaPublicacion;
    }

    public void setNumeroPaginas(int numeroPaginas) {
        this.numeroPaginas = numeroPaginas;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }


    //toString


    @Override
    public String toString() {
        return "Texto{" +
                "titulo='" + titulo + '\'' +
                ", autor='" + autor + '\'' +
                ", fechaPublicacion=" + fechaPublicacion +
                ", numeroPaginas=" + numeroPaginas +
                ", idioma='" + idioma + '\'' +
                ", signatura='" + signatura + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido='" + restringido + '\'' +
                ", tema='" + tema + '\'' +
                '}';
    }
}
