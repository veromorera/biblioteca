package bl;

import java.time.LocalDate;

public class Audio extends Material {

    private String formato;
    private double duracion;
    private String idioma;


    //constructor
    public Audio(String signatura, LocalDate fechaCompra, String restringido, String tema) {
        super(signatura, fechaCompra, restringido, tema);
    }

    //getters
    public String getFormato() {
        return formato;
    }
    public double getDuracion() {
        return duracion;
    }
    public String getIdioma() {
        return idioma;
    }


    //setters
    public void setFormato(String formato) {
        this.formato = formato;
    }
    public void setDuracion(double duracion) {
        this.duracion = duracion;
    }
    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }


    //toString
    @Override
    public String toString() {
        return "Audio{" +
                "signatura='" + signatura + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido='" + restringido + '\'' +
                ", tema='" + tema + '\'' +
                ", formato='" + formato + '\'' +
                ", duracion=" + duracion +
                ", idioma='" + idioma + '\'' +
                '}';
    }
}
