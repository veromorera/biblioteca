package bl;

public class Administrativo extends Persona {

    private String tipoNombramiento;
    private double horasSemanales;

    //constructor
    public Administrativo(int id, String nombre, String apellido, String tipoNombramiento, double horasSemanales) {
        super(id, nombre, apellido);

        this.tipoNombramiento = tipoNombramiento;
        this.horasSemanales = horasSemanales;
    }


    //getters
    public String getTipoNombramiento() {
        return tipoNombramiento;
    }
    public double getHorasSemanales() {
        return horasSemanales;
    }


    //setters
    public void setTipoNombramiento(String tipoNombramiento) {
        this.tipoNombramiento = tipoNombramiento;
    }
    public void setHorasSemanales(double horasSemanales) {
        this.horasSemanales = horasSemanales;
    }


    //toString
    @Override
    public String toString() {
        return "Administrativo{" +
                "id=" + getId() +  '\'' +
                ", nombre=" + getNombre() +  '\'' +
                ", apellido=" + getApellido() +  '\'' +
                ", tipoNombramiento=" + tipoNombramiento +
                ", horasSemanales=" + horasSemanales +
                '}';
    }
}
