package bl;

import java.time.LocalDate;

public class Material {

    protected String signatura;
    protected LocalDate fechaCompra;
    protected String restringido;
    protected String tema;


    //constructor
    public Material(String signatura, LocalDate fechaCompra, String restringido, String tema) {
        this.signatura = signatura;
        this.fechaCompra = fechaCompra;
        this.restringido = restringido;
        this.tema = tema;
    }


    //getters
    public String getSignatura() {
        return signatura;
    }
    public LocalDate getFechaCompra() {
        return fechaCompra;
    }
    public String getRestringido() {
        return restringido;
    }
    public String getTema() {
        return tema;
    }


    //setters
    public void setSignatura(String signatura) {
        this.signatura = signatura;
    }
    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }
    public void setRestringido(String restringido) {
        this.restringido = restringido;
    }
    public void setTema(String tema) {
        this.tema = tema;
    }


    //toString
    @Override
    public String toString() {
        return "Material{" +
                "signatura='" + signatura + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido='" + restringido + '\'' +
                ", tema='" + tema + '\'' +
                '}';
    }
}
