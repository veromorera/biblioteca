package bl;

import java.time.LocalDate;

public class Otros extends Material {


    //atributos
    private String descripcion;

    //constructor
    public Otros(String signatura, LocalDate fechaCompra, String restringido, String tema, String descripcion) {
        super(signatura, fechaCompra, restringido, tema);

        this.descripcion = descripcion;
    }

    //getters
    public String getDescripcion() {
        return descripcion;
    }

    //setters
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    //toString
    @Override
    public String toString() {
        return "Otros{" +
                "signatura='" + signatura + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido='" + restringido + '\'' +
                ", tema='" + tema + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
