package bl;

import java.time.LocalDate;

public class Video extends Material {

    private String formato;
    private double duracion;
    private String idioma;
    private String director;


    //constructor
    public Video(String signatura, LocalDate fechaCompra, String restringido, String tema) {
        super(signatura, fechaCompra, restringido, tema);
    }

    //getters
    public String getFormato() {
        return formato;
    }

    public double getDuracion() {
        return duracion;
    }

    public String getIdioma() {
        return idioma;
    }

    public String getDirector() {
        return director;
    }


    //setters
    public void setFormato(String formato) {
        this.formato = formato;
    }

    public void setDuracion(double duracion) {
        this.duracion = duracion;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public void setDirector(String director) {
        this.director = director;
    }


    //toString
    @Override
    public String toString() {
        return "Video{" +
                "signatura='" + signatura + '\'' +
                ", fechaCompra=" + fechaCompra +
                ", restringido='" + restringido + '\'' +
                ", tema='" + tema + '\'' +
                ", formato='" + formato + '\'' +
                ", duracion=" + duracion +
                ", idioma='" + idioma + '\'' +
                ", director='" + director + '\'' +
                '}';
    }
}
