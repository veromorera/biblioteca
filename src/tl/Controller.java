/*
 * Descripcion: CONTROLLER - Lab 3
 * Fecha: 20-06-2019
 * Autor: Veronica Morera Carballo.
 * Fecha de modificacion: 20-6-2019
 */



package tl;

import bl.Administrativo;
import bl.Estudiante;
import bl.Persona;
import bl.Profesor;
import dl.CL;

import java.time.LocalDate;

public class Controller {

    public CL logica = new CL();

    //---------------------------------------------------------------------------------------//
    public void registroPersona(int pid, String pnombre, String papellido) {

        //nueva persona (instancia)
        Persona nueva_persona = new Persona(pid, pnombre, papellido);

        //convertir persona en lista
        String persona = nueva_persona.toString();

        //registrar persona
        logica.registrarPersona(persona);
        System.out.println("Persona registrada exitosamente." + "\n");
    }

    public void registroEstudiante(int pid, String pnombre, String papellido,
                                   String pcarrera, int pcreditos) {

        //nuevo estudiante (instancia)
        Persona nuevo_estudiante = new Estudiante(pid, pnombre, papellido, pcarrera, pcreditos);

        //convertir estudiante en lista
        String estudiante = nuevo_estudiante.toString();

        //registrar estudiante
        logica.registrarPersona(estudiante);
        System.out.println("Estudiante registrado exitosamente." + "\n");
    }

    public void registroProfesor(int pid, String pnombre, String papellido,
                                 String ptipoContrato, LocalDate pfechaContrato) {

        //nuevo profesor (instancia)
        Persona nuevo_profesor = new Profesor(pid, pnombre, papellido, ptipoContrato, pfechaContrato);

        //convertir profe en lista
        String profe = nuevo_profesor.toString();

        //registrar profesor
        logica.registrarPersona(profe);
        System.out.println("Profesor registrado exitosamente." + "\n");

    }

    public void registroAdministrativo(int pid, String pnombre, String papellido,
                                       String ptipoNombramiento, int phorasSemanales) {


        //nuevo administrativo (instancia)
        Persona nuevo_administrativo = new Administrativo(pid, pnombre, papellido,
                ptipoNombramiento, phorasSemanales);

        //convertir admin en lista
        String admin = nuevo_administrativo.toString();

        //registrar administrativo
        logica.registrarPersona(admin);

        System.out.println("Administrativo registrado exitosamente." + "\n");
    }

    //---------------------------------------------------------------------------------------//
    public void listaPersonas(int pmenu) {
        logica.listarPersonas(pmenu);
        System.out.println();
    }

    //---------------------------------------------------------------------------------------//
    public boolean repetidos(int pid) {
        return logica.personasRepetidas(pid);
    }

    public boolean busquedaPorId(int pid) {
        return logica.buscarPorId(pid);
    }

    //---------------------------------------------------------------------------------------//
    public void agregarPersonasDef(){
        logica.personasDefault();
    }


}
