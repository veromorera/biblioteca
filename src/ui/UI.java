/*
 * Descripcion: UI - Lab 3
 * Fecha: 19-06-2019
 * Autor: Veronica Morera Carballo.
 * Fecha de modificacion: 19-6-2019
 */
package ui;

import tl.Controller;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.ArrayList;

public class UI {

    static Controller gestor = new Controller();

    public static void main(String[] args) throws java.io.IOException {

        int menu;
        boolean salir = false;
        BufferedReader teclado = new BufferedReader(new InputStreamReader(System.in));


        //Agregar instancias de relleno
        gestor.agregarPersonasDef();

        while (!salir) {

            System.out.println("Bienvenido al registro de Biblioteca Universitaria." + "\n"
                    + "Por favor elija una opción del menú: " + "\n"
                    + "\n"
                    + "1 - Registrar Persona." + "\n"
                    + "2 - Listar Personas." + "\n"
                    + "3 - Registrar Estudiante." + "\n"
                    + "4 - Listar Estudiantes." + "\n"
                    + "5 - Registrar Profesor." + "\n"
                    + "6 - Listar Profesores." + "\n"
                    + "7 - Registrar Administrativo." + "\n"
                    + "8 - Listar Administrativos." + "\n"
                    + "9 - Listar Carrera de estudiante" + "\n"
                    + "10 - Salir de la aplicación.");

            menu = Integer.parseInt(teclado.readLine());

            switch (menu) {

                case 1:

                    //variables para nueva persona:
                    int id;
                    String nombre;
                    String apellido;

                    System.out.println("Bienvenido a la herramienta de registrar persona" + "\n");

                    System.out.println("ingrese la IDENTIFICACION de la persona:");
                    id = Integer.parseInt(teclado.readLine());

                    //verificar repetidos usando el id
                    if (gestor.repetidos(id)) {
                        System.out.println("No se puede registrar. La persona ya está en el sistema"  + "\n");
                        break;

                    } else {
                        System.out.println("ingrese el NOMBRE de la persona:");
                        nombre = teclado.readLine();

                        System.out.println("ingrese el APELLIDO de la persona:");
                        apellido = teclado.readLine();

                        //registrar persona
                        gestor.registroPersona(id, nombre, apellido);
                    }
                    break;


                case 2:
                    System.out.println("Bienvenido a la herramienta de listar persona" + "\n");
                    gestor.listaPersonas(2);
                    break;


                case 3:
                    //variables para nuevo estudiante:

                    String carrera;
                    int creditos;

                    System.out.println("Bienvenido a la herramienta de registrar estudiante" + "\n");

                    System.out.println("ingrese la IDENTIFICACION del estudiante:");
                    id = Integer.parseInt(teclado.readLine());

                    //verificar repetidos usando el id
                    if (gestor.repetidos(id)) {
                        System.out.println("No se puede registrar. El estudiante ya está en el sistema" + "\n");
                        break;

                    } else {
                        System.out.println("ingrese el NOMBRE del estudiante:");
                        nombre = teclado.readLine();

                        System.out.println("ingrese el APELLIDO del estudiante:");
                        apellido = teclado.readLine();

                        System.out.println("ingrese la CARRERA del estudiante:");
                        carrera = teclado.readLine();

                        System.out.println("ingrese los CREDITOS del estudiante:");
                        creditos = Integer.parseInt(teclado.readLine());

                        //registrar estudiante
                        gestor.registroEstudiante(id, nombre, apellido, carrera, creditos);
                    }
                    break;


                case 4:
                    System.out.println("Bienvenido a la herramienta de listar estudiante" + "\n");
                    gestor.listaPersonas(4);
                    break;


                case 5:
                    //variables para nuevo profesor:

                    String tipoContrato;
                    LocalDate fechaContrato;

                    System.out.println("Bienvenido a la herramienta de registrar profesor" + "\n");

                    System.out.println("ingrese la IDENTIFICACION del profesor:");
                    id = Integer.parseInt(teclado.readLine());

                    //verificar repetidos usando el id
                    if (gestor.repetidos(id)) {
                        System.out.println("No se puede registrar. El profesor ya está en el sistema" + "\n");
                        break;

                    } else {
                        System.out.println("ingrese el NOMBRE del profesor:");
                        nombre = teclado.readLine();

                        System.out.println("ingrese el APELLIDO del profesor:");
                        apellido = teclado.readLine();

                        System.out.println("ingrese el TIPO DE CONTRATO del profesor:");
                        tipoContrato = teclado.readLine();

                        System.out.println("ingrese la FECHA DE CONTRATO del profesor: ('yyyy-MM-dd' ) ");
                        String fechaContratoStr = teclado.readLine();

                        //convertir fecha a formato fecha
                        fechaContrato = LocalDate.parse(fechaContratoStr);

                        //registrar profesor
                        gestor.registroProfesor(id, nombre, apellido, tipoContrato, fechaContrato);
                    }
                    break;


                case 6:
                    System.out.println("Bienvenido a la herramienta de listar profesor" + "\n");
                    gestor.listaPersonas(6);
                    break;

                case 7:
                    //variables para nuevo estudiante:

                    String tipoNombramiento;
                    int horasSemanales;

                    System.out.println("Bienvenido a la herramienta de registrar administrativo" + "\n");

                    System.out.println("ingrese la IDENTIFICACION del administrativo:");
                    id = Integer.parseInt(teclado.readLine());

                    //verificar repetidos usando el id
                    if (gestor.repetidos(id)) {
                        System.out.println("No se puede registrar. El administrativo ya está en el sistema" + "\n");
                        break;

                    } else {
                        System.out.println("ingrese el NOMBRE del administrativo:");
                        nombre = teclado.readLine();

                        System.out.println("ingrese el APELLIDO del administrativo:");
                        apellido = teclado.readLine();

                        System.out.println("ingrese el TIPO DE NOMBRAMIENTO del administrativo: (A, B, C)");
                        tipoNombramiento = teclado.readLine();

                        System.out.println("ingrese las HORAS SEMANALES del administrativo:");
                        horasSemanales = Integer.parseInt(teclado.readLine());

                        //registrar administragtivo
                        gestor.registroAdministrativo(id, nombre, apellido, tipoNombramiento, horasSemanales);
                    }
                    break;

                case 8:
                    System.out.println("Bienvenido a la herramienta de listar administrativos" + "\n");
                    gestor.listaPersonas(8);
                    break;

                case 9:
                    System.out.println("Bienvenido a la herramienta de listar carreras" + "\n");

                    System.out.println("ingrese la IDENTIFICACION del estudiante:");
                    id = Integer.parseInt(teclado.readLine());

                    //encontrar estudiante
                    if (gestor.busquedaPorId(id)) {
                        break;
                    } else {
                        System.out.println("El estudiante no se encuentra en el sistema" + "\n");
                    }
                    break;

                case 10:
                    System.out.println("¡Hasta luego! Gracias por utilizar esta aplicación." + "\n");
                    salir = true;
                    break;

                default:
                    System.out.println("Error, por favor introduzca una opción valida" + "\n");

            }
        }
    }
}