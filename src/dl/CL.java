/*
 * Descripcion: CAPA LOGICA - Lab 3
 * Fecha: 19-06-2019
 * Autor: Veronica Morera Carballo.
 * Fecha de modificacion: 19-6-2019
 */


package dl;

import bl.Estudiante;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class CL {


    static ArrayList<String> listaPersonas = new ArrayList<>();


    //PERSONAS DE RELLENO
    public void personasDefault() {

        String p1 = "Persona{id=123, nombre='Juan', apellido='Perez'}";
        String p2 = "Estudiante{id=234', nombre=Veronica', apellido=Morera', carrera='Software', creditos=6}";
        String p3 = "Profesor{id=456', nombre=Alvaro', apellido=Cordero', tipoContrato=Medio Tiempo, fechaContrato=2010-05-01}";
        String p4 = "Administrativo{id=567', nombre=Kathia', apellido=Vega', tipoNombramiento=A, horasSemanales=28.0}";

        listaPersonas.add(p1);
        listaPersonas.add(p2);
        listaPersonas.add(p3);
        listaPersonas.add(p4);
    }
    //---------------------------------------------------------------------------------------//

    //REGISTRAR PERSONAS
    public ArrayList registrarPersona(String ppersona) {
        listaPersonas.add(ppersona);
        return listaPersonas;
    }

    //---------------------------------------------------------------------------------------//

    //LISTAR PERSONAS
    public ArrayList listarPersonas(int menu) {

        for (String personas : listaPersonas) {


            switch (menu) {
                case 2:
                    System.out.println(personas);
                    break;
                case 4:
                    if (personas.contains("Estudiante")) {
                        System.out.println(personas);
                    }
                    break;
                case 6:
                    if (personas.contains("Profesor")) {
                        System.out.println(personas);
                    }
                    break;
                case 8:
                    if (personas.contains("Administrativo")) {
                        System.out.println(personas);
                        break;
                    }
                default:
                    break;
            }

        }
        return listaPersonas;
    }

    //---------------------------------------------------------------------------------------//

    //BUSCAR REPETIDOS
    public boolean personasRepetidas(int pid) {

        String cedula = Integer.toString(pid);
        boolean repetido = false;

        for (String personas : listaPersonas) {

            //separar cada persona en listas
            String[] arrPersona = personas.split(",");

            //separar la info personal de cada cliente
            for (String persona : arrPersona) {
                persona.split(",");

                if (persona.contains(cedula)) {
                    repetido = true;
                }
            }
        }

        return repetido;
    }


    //BUSCAR POR ID
    public boolean buscarPorId(int pid) {

        String cedula = Integer.toString(pid);
        String carrera = "";
        boolean existe = false;

        for (String personas : listaPersonas) {

            //separar cada persona en listas
            String[] arrPersona = personas.split(",");

            //separar la info personal de cada cliente
            for (String persona : arrPersona) {
                persona.split(",");

                if (persona.contains(cedula) && persona.contains("Estudiante")) {

                    //aqui estoy haciendo un aproach de "slice" estilo Python, pero en Java
                    String[] soloCarrera = Arrays.copyOfRange(arrPersona, 3, 4);

                    /*Nota Vero: copyOfRange selecciona por indices de inicio y final...
                     si quisiera que agarre desde el indice N hasta el final, pongo algo como:
                     String[] soloCarrera = Arrays.copyOfRange(arrPersona, 3, arrPersona.lenght);
                     */

                    System.out.println(Arrays.toString(soloCarrera));
                    existe = true;
                }
            }
        }
        return existe;
    }
}
